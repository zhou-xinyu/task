package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Score {
	public static void main(String[] args) throws IOException {

		// 导入总分的配置文件
		Properties total = new Properties();
		total.load(new FileInputStream("total.properties"));

		// 导入大小班课资源
		File small = new File("small.html");
		Document docSmall = Jsoup.parse(small, "UTF-8");
		File all = new File("all.html");
		Document docAll = Jsoup.parse(all, "UTF-8");

		// 课堂完成部分分数计算
		String totalBase = total.getProperty("base");
		double baseScore = 0;
		// 读取内容
		Elements baseDoc = docSmall.select(".interaction-row");
		for (int i = 0; i < baseDoc.size(); i++) {
			if (baseDoc.get(i).text().toString().contains("课堂完成部分")) {
				// 非红色部分才有分数
				if (!baseDoc.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(baseDoc.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						baseScore += sc;
					}
				}
			}
		}

		// 课堂小测部分分数计算
		String totalTest = total.getProperty("test");
		double testScore = 0;
		// 读取内容
		Elements testDoc = docSmall.select(".interaction-row");
		for (int i = 0; i < testDoc.size(); i++) {
			if (testDoc.get(i).text().toString().contains("课堂小测")) {
				// 非红色部分才有分数
				if (!testDoc.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(testDoc.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						testScore += sc;
					}
				}
			}
		}

		// 课前自测部分分数计算
		String totalBefore = total.getProperty("before");
		double beforeScore = 0;
		// 读取内容
		Elements beforeDocAll = docAll.select(".interaction-row");
		Elements beforeDocSmall = docSmall.select(".interaction-row");
		for (int i = 0; i < beforeDocAll.size(); i++) {
			if (beforeDocAll.get(i).text().toString().contains("课前自测")) {
				// 非红色部分才有分数
				if (!beforeDocAll.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(beforeDocAll.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						beforeScore += sc;
					}
				}
			}
		}
		for (int i = 0; i < beforeDocSmall.size(); i++) {
			if (beforeDocSmall.get(i).text().toString().contains("课前自测")) {
				// 非红色部分才有分数
				if (!beforeDocSmall.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(beforeDocSmall.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						beforeScore += sc;
					}
				}
			}
		}

		// 编程题部分分数计算
		String totalProgram = total.getProperty("program");
		double programScore = 0;
		// 读取内容
		Elements programDoc = docSmall.select(".interaction-row");
		for (int i = 0; i < programDoc.size(); i++) {
			if (programDoc.get(i).text().toString().contains("编程题")) {
				// 非红色部分才有分数
				if (!programDoc.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(programDoc.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						programScore += sc;
					}
				}
			}
		}
		
		// 附加题部分分数计算
		String totalAdd = total.getProperty("add");
		double addScore = 0;
		// 读取内容
		Elements addDoc = docSmall.select(".interaction-row");
		for (int i = 0; i < addDoc.size(); i++) {
			if (addDoc.get(i).text().toString().contains("附加题")) {
				// 非红色部分才有分数
				if (!addDoc.get(i).toString().contains("#EC6941")) {
					// 得到 “xx 经验”部分
					String pattern = "\\w* 经验";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(addDoc.get(i).text().toString());
					while (m.find()) {
						int sc = Integer.parseInt(m.group().replaceAll(" ", "").replaceAll("经验", "").toString());
						addScore += sc;
					}
				}
			}
		}

		// 最终的各项得分
		double sumBase = (baseScore/Integer.parseInt(totalBase))*100*0.95;
		double sumTest = (testScore/Integer.parseInt(totalTest))*100;
		double sumBefore = (beforeScore/Integer.parseInt(totalBefore))*100;
		double sumProgram = (programScore/Integer.parseInt(totalProgram))*95;
		double sumAdd = (addScore/Integer.parseInt(totalAdd))*90;
		
		double sum = sumBase*0.3+sumTest*0.2+sumBefore*0.25+sumProgram*0.1+sumAdd*0.05;
		System.out.println(sum);
	}
}